import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SuiteDisplayName("Junit 5 Suite Demo")
@SelectPackages({"com.rostyslavprotsiv"})
@IncludeClassNamePatterns({"^.*$"})
public class AllTests {
}
