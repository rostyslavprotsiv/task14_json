package com.rostyslavprotsiv.model.action;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JSONActionTest {
    private final String parsed = "Flower{name='Almond', soil=GROUND, " +
            "origin='USA', visualParameters=VisualParameter{stalkColor='Red'," +
            " leafsColor='Yellow', averageSize=10}, " +
            "growingTips=GrowingTips{temperatureInDegrees=45, lighting=false," +
            " wateringPerWeek=20}, multiplying=ENGRAFTMENT}\n" +
            "Flower{name='Marigold', soil=GROUND, origin='USA', " +
            "visualParameters=VisualParameter{stalkColor='Blue', " +
            "leafsColor='Yellow', averageSize=44}, " +
            "growingTips=GrowingTips{temperatureInDegrees=32, lighting=false," +
            " wateringPerWeek=37}, multiplying=ENGRAFTMENT}\n" +
            "Flower{name='Rose', soil=GROUND, origin='Ukraine', " +
            "visualParameters=VisualParameter{stalkColor='Green', " +
            "leafsColor='Green', averageSize=15}, " +
            "growingTips=GrowingTips{temperatureInDegrees=10, lighting=true, " +
            "wateringPerWeek=20}, multiplying=LEAF}\n" +
            "Flower{name='Sunflower', soil=SODPODZOLIC, origin='Belarus', " +
            "visualParameters=VisualParameter{stalkColor='Green', " +
            "leafsColor='Blue', averageSize=23}, " +
            "growingTips=GrowingTips{temperatureInDegrees=75, lighting=true, " +
            "wateringPerWeek=67}, multiplying=LEAF}\n" +
            "Flower{name='Tulip', soil=SODPODZOLIC, origin='France', " +
            "visualParameters=VisualParameter{stalkColor='Red', " +
            "leafsColor='Green', averageSize=150}, " +
            "growingTips=GrowingTips{temperatureInDegrees=25, lighting=true, " +
            "wateringPerWeek=200}, multiplying=LEAF}\n" +
            "Flower{name='Zinnia', soil=PODZOLIC, origin='France', " +
            "visualParameters=VisualParameter{stalkColor='Black', " +
            "leafsColor='Blue', averageSize=48}, " +
            "growingTips=GrowingTips{temperatureInDegrees=59, lighting=true, " +
            "wateringPerWeek=32}, multiplying=SEED}\n";

    @Test
    public void testWithGSON() {
        JSONAction action = new JSONAction();
        assertEquals(action.parseWithGSON(), parsed);
    }
}
