package com.rostyslavprotsiv.model.exceptions;

public class GrowingTipsLogicException extends Exception {
    public GrowingTipsLogicException() {
    }

    public GrowingTipsLogicException(String s) {
        super(s);
    }

    public GrowingTipsLogicException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public GrowingTipsLogicException(Throwable throwable) {
        super(throwable);
    }

    public GrowingTipsLogicException(String s, Throwable throwable, boolean b
            , boolean b1) {
        super(s, throwable, b, b1);
    }
}
