package com.rostyslavprotsiv.model.exceptions;

public class VisualParametersLogicException extends Exception {
    public VisualParametersLogicException() {
    }

    public VisualParametersLogicException(String s) {
        super(s);
    }

    public VisualParametersLogicException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public VisualParametersLogicException(Throwable throwable) {
        super(throwable);
    }

    public VisualParametersLogicException(String s, Throwable throwable,
                                         boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
