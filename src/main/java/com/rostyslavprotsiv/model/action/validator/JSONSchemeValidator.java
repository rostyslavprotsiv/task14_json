package com.rostyslavprotsiv.model.action.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class JSONSchemeValidator {

    public static boolean validateJSON(File jsonPath, File schemaPath) {
        final JsonNode data;
        final JsonNode schema;
        final ProcessingReport report;
        try {
            data = JsonLoader.fromFile(jsonPath);
            schema = JsonLoader.fromFile(schemaPath);
            final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
            JsonValidator validator = factory.getValidator();
            report = validator.validate(schema, data);
            return report.isSuccess();
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
        return false;
    }
}
