package com.rostyslavprotsiv.model.action.parsers.gson;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.rostyslavprotsiv.model.entity.Flower;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSONGSONParser {
    private Gson gson;

    public JSONGSONParser() {
        this.gson = new Gson();
    }

    public List<Flower> getFlowerList(File jsonFile) {
        List<Flower> flowers = new ArrayList<>();
        try {
            flowers = Arrays.asList(gson.fromJson(
                    new JsonReader(new FileReader(jsonFile)), Flower[].class));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return flowers;
    }
}
