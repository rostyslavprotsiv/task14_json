package com.rostyslavprotsiv.model.action.parsers.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rostyslavprotsiv.model.entity.Flower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSONJacksonParser {
    private final Logger LOGGER = LogManager.getLogger(JSONJacksonParser.class);
    private ObjectMapper objectMapper;

    public JSONJacksonParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Flower> getFlowerList(File jsonFile) {
        List<Flower> flowers = new ArrayList<>();
        try {
            flowers = Arrays
                    .asList(objectMapper.readValue(jsonFile, Flower[].class));
        } catch (IOException e) {
            String logMessage = e.getMessage();
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
            e.printStackTrace();
        }
        return flowers;
    }
}
