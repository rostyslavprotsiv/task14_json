package com.rostyslavprotsiv.model.action.resourcemanager;

import java.util.Locale;
import java.util.ResourceBundle;

public enum ResourceManager {
    INSTANCE;
    private ResourceBundle resourceBundle;
    private final String resourceName = "paths";

    ResourceManager() {
        resourceBundle = ResourceBundle.getBundle(resourceName,
                Locale.getDefault());
    }

    public String getString(final String key) {
        return resourceBundle.getString(key);
    }
}
