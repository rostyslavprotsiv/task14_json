package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.action.comparator.FlowerComparator;
import com.rostyslavprotsiv.model.action.parsers.gson.JSONGSONParser;
import com.rostyslavprotsiv.model.action.parsers.jackson.JSONJacksonParser;
import com.rostyslavprotsiv.model.action.resourcemanager.ResourceManager;
import com.rostyslavprotsiv.model.action.validator.JSONSchemeValidator;
import com.rostyslavprotsiv.model.entity.Flower;

import java.io.File;
import java.util.List;

public class JSONAction {
    private final File JSON = new File(ResourceManager.INSTANCE
            .getString("json"));
    private final File JSON_SCHEME = new File(ResourceManager.INSTANCE
            .getString("jsonSchema"));

    public String parseWithGSON() {
        StringBuilder parsed = new StringBuilder();
        List<Flower> parsedList = new JSONGSONParser().getFlowerList(JSON);
        parsedList.sort(new FlowerComparator());
        parsedList.forEach(s -> parsed.append(s).append("\n"));
        return parsed.toString();
    }

    public String parseWithJackson() {
        StringBuilder parsed = new StringBuilder();
        List<Flower> parsedList = new JSONJacksonParser().getFlowerList(JSON);
        parsedList.sort(new FlowerComparator());
        parsedList.forEach(s -> parsed.append(s).append("\n"));
        return parsed.toString();
    }

    public boolean validate() {
        return JSONSchemeValidator.validateJSON(JSON, JSON_SCHEME);
    }
}
