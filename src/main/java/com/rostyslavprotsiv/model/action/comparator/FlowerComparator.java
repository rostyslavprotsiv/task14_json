package com.rostyslavprotsiv.model.action.comparator;

import com.rostyslavprotsiv.model.entity.Flower;

import java.util.Comparator;

public class FlowerComparator implements Comparator<Flower> {
    @Override
    public int compare(Flower flower, Flower flower1) {
        return flower.getName().compareTo(flower1.getName());
    }
}
