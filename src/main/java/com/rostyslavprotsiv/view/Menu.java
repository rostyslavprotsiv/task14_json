package com.rostyslavprotsiv.view;

public final class Menu extends AbstractMenu {

    public Menu() {
        menu.put(0, "Parse with GSON");
        menu.put(1, "Parse with Jackson");
        menu.put(2, "Validate(with Jackson)");
        menu.put(3, "Exit");
        methodsForMenu.put(0, this::printParsedWithGSON);
        methodsForMenu.put(1, this::printParsedWithJackson);
        methodsForMenu.put(2, this::printValidated);
        methodsForMenu.put(3, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for task14_JSON");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void printParsedWithGSON() {
        LOGGER.info(CONTROLLER.getParsedWithGSON());
    }

    private void printParsedWithJackson() {
        String parsed = CONTROLLER.getParsedWithJackson();
        if (parsed == null) {
            String logMessage = "Error with parsing with Jackson";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
        } else {
            LOGGER.info(parsed);
        }
    }

    private void printValidated() {
        LOGGER.info("Was validated : ");
        LOGGER.info(CONTROLLER.validate());
    }
}
