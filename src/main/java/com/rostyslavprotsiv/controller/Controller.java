package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.JSONAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final JSONAction JSON_ACTION = new JSONAction();

    public String getParsedWithGSON() {
        return JSON_ACTION.parseWithGSON();
    }

    public String getParsedWithJackson() {
        return JSON_ACTION.parseWithJackson();
    }

    public boolean validate() {
        boolean wasValidated = JSON_ACTION.validate();
        if (wasValidated) {
            return true;
        } else {
            String logMessage = "Error with validating";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            return false;
        }
    }
}
